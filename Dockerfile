# FROM fedora:latest
# RUN dnf update -y && dnf install -y nginx git
# RUN git clone https://github.com/jumozaga/landpage_addams.git /var/www/html/
# WORKDIR /var/www/html/
# RUN chmod +x /var/www/html/landpage_addams
# EXPOSE 80
# CMD ["nginx", "-g", "daemon off;"]

FROM nginx:latest
WORKDIR /usr/share/nginx/html
COPY  landpage_addams /usr/share/nginx/html
EXPOSE 80
