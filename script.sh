#!/bin/bash

echo "Atualizando o S.O..."
sudo apt-get update
sudo apt-get upgrade -y

echo "Instalando o nginx..."
sudo apt install nginx -y
sudo systemctl start nginx

echo "Baixando os arquivos da aplicação..."
sudo git clone https://github.com/denilsonbonatti/mundo-invertido.git

echo "Movendo a pasta do projeto para a pasta do ngnix"
sudo cp mundo-invertido -R /var/www/html

echo "Instalando dependencias..."
sudo apt-get install --reinstall xdg-utils

echo "Encontrando o ip..."
ip a
IPV6=$(ip -6 addr show enps08 | awk '/inet6/{print $2}' | cut -d/ -f1)
IPV4=$(ip addr show enp0s8 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')
echo $IPV4
echo $IPV6
echo "Abrindo Navegador"
xdg-open http://$(ip addr show enp0s8 | grep -oP '(?<=inet\s)\d+(\.\d+){3}')/mundo-invertido
xdg-open http://[$(ip -6 addr show enp0s8 | grep -i global | awk '/inet6/{print $2}' | cut -d/ -f1)]/mundo-invertido

echo "Instalando o Docker"
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

echo "Instalando o GitLab-Runner"
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner

echo " Registar e iniciar o GitLab-Runner"
sudo gitlab-runner register
sudo gitlab-runner start

echo "Dando permissões para uso do Docker para o usuário gitlab-runner"
sudo usermod -G docker gitlab-runner
sudo passwd gitlab-runner
su gitlab-runner

echo "Logando na conta do Docker Hub"
docker login -u jumozaga
